<?php


namespace App\Services;


use App\Entity\Node;

class BinaryTreeService
{

    public function printPaths(Node $root) {
        $path = [];
        $this->printPathsRecursive($root, $path, 0);
    }

    public function printPathsRecursive (Node $node, $path, $len) {
        if (is_null($node)) {
            return;
        }
        $path[$len] = $node->getValue();
        $len++;

      if (is_null($node->getLeft()) && is_null($node->getRight()))
      {
          $this->displayPath(path, pathLen);
      }
      else
      {
          $this->printPathsRecursive($node->getLeft(), $path, $len);
          $this->printPathsRecursive($node->getRight(), $path, $len);
      }
    }

    public function displayPath($path, $len) {
        $signPost = '[ ';
        for ($i=0; $i<$len; $i++) {
            $signPost .= $path[$i].' ';
        }
        return $signPost. ']';
    }


}