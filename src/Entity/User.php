<?php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $fullName;

    /**
     * @ORM\Column(type="string")
     */
    protected $email2;

    /**
     * @ORM\Column(type="string")
     */
    private $telMobile;

    /**
     * @ORM\Column(type="string")
     */
    private $telMobile2;

    /**
     * @ORM\Column(type="string")
     */
    private $telPro;

    /**
     * @ORM\Column(type="string")
     */
    private $fax;

    /**
     * @ORM\Column(type="string")
     */
    private $sexe;

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * @param mixed $email2
     */
    public function setEmail2($email2): void
    {
        $this->email2 = $email2;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     */
    public function setFullName($fullName): void
    {
        $this->fullName = $fullName;
    }

    /**
     * @return mixed
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param mixed $fax
     */
    public function setFax($fax): void
    {
        $this->fax = $fax;
    }

    /**
     * @return mixed
     */
    public function getTelMobile()
    {
        return $this->telMobile;
    }

    /**
     * @param mixed $telMobile
     */
    public function setTelMobile($telMobile): void
    {
        $this->telMobile = $telMobile;
    }

    /**
     * @return mixed
     */
    public function getTelMobile2()
    {
        return $this->telMobile2;
    }

    /**
     * @param mixed $telMobile2
     */
    public function setTelMobile2($telMobile2): void
    {
        $this->telMobile2 = $telMobile2;
    }

    /**
     * @return mixed
     */
    public function getTelPro()
    {
        return $this->telPro;
    }

    /**
     * @param mixed $telPro
     */
    public function setTelPro($telPro): void
    {
        $this->telPro = $telPro;
    }

    /**
     * @return mixed
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * @param mixed $sexe
     */
    public function setSexe($sexe): void
    {
        $this->sexe = $sexe;
    }


    public function __construct()
    {
        parent::__construct();
    }
}